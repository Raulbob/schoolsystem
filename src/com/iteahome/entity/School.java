package com.iteahome.entity;

import java.util.ArrayList;

/**
 * Class that describes a school and its attributes.
 */
public class School {

    private String name;
    private String address;
    private ArrayList<Person> personList;

    public School(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public School() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }

}
