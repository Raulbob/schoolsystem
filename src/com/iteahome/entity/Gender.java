package com.iteahome.entity;

/**
 * Enums are classes destined for describing constants. This one holds the binary genders.
 */
public enum Gender {

    FEMININE, MASCULINE

}
