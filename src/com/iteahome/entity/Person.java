package com.iteahome.entity;

/**
 * Class that describes a person and its attributes. It's an abstract class because we don't want this one to be instantiated by anyone. It's here just to be a parent for others.
 */
public abstract class Person {

    private String name;
    private String email;
    private int age;
    private Gender gender;

    public Person() {
    }

    public Person(String name, String email, int age, Gender gender) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    //Creeaza un string ce contine toate field-urile din clasa
    public String pretty() {
        return name + "," + email + "," + age + "," + gender;
    }

}
