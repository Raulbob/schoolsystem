package com.iteahome.entity;

/**
 * Teacher holds information describing a person that also teaches a discipline.
 */
public class Teacher extends Person {

    private String discipline;

    public Teacher(String name, String email, int age, Gender gender, String discipline) {
        super(name, email, age, gender);
        this.discipline = discipline;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    @Override
    public String pretty() {
        return super.pretty() + "," + discipline + " ----> TEACHER";
    }

}
