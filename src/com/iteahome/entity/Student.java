package com.iteahome.entity;

/**
 * Student holds information describing a person that also studies at the school.
 */
public class Student extends Person {

    private int yearOfStudy;

    public Student(String name, String email, int age, Gender gender, int yearOfStudy) {
        super(name, email, age, gender);
        this.yearOfStudy = yearOfStudy;
//        super();
//        setName(name);
//        setEmail(email);
//        setAge(age);
//        setGender(gender);
//        this.yearOfStudy = yearOfStudy;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    @Override
    public String pretty() {
        return super.pretty() + "," + yearOfStudy + "----> Student";
    }

}
