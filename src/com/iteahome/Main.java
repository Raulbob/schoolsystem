package com.iteahome;

import com.iteahome.entity.*;

import java.util.ArrayList;

/**
 * Main class. Used for having an entry point where we can create our objects and solve the Client's problem.
 */
public class Main {

    public static void main(String[] args) {
        System.err.println("\n____________1. PRINT ALL SCHOOLS___________________________");

        //CREATE OBJECTS.
        School schoolAux = new School("Tiberiu Popovici", "Calea Turzii 140-142");
        School school = new School("Avram Iancu", "Strada Onisifor Ghibu 33");

        //CREATE LIST OF SCHOOLS.
        ArrayList<School> schools = new ArrayList<>();
        schools.add(schoolAux);
        schools.add(school);

        //PRINT ALL SCHOOLS.
        printAllSchools(schools);

        System.err.println("\n____________2. ALL PERSONS FROM AVRAM IANCU_________________________");

        //CREATE PERSONS.
        Person person3 = new Student("person3", "person3@email.com", 17, Gender.MASCULINE, 3);
        Person person4 = new Student("person4", "person4@email.com", 18, Gender.FEMININE, 4);

        Person person5 = new Teacher("person5", "person5@email.com", 28, Gender.FEMININE, "English");

        //CREATE LIST OF PERSONS.
        ArrayList<Person> personList = new ArrayList<>();
        personList.add(person3);
        personList.add(person4);
        personList.add(person5);

        //ADD THE LIST OF PERSONS IN A SCHOOL.
        school.setPersonList(personList);

        //PRINT ALL PERSONS IN SCHOOL.
        printAllPersons(school);

        System.err.println("\n____________3. ALL STUDENTS && TEACHERS  __________________");
        printAllStudentsAndAllTeachers(school);
    }

    /**
     * A method that prints information about the School objects added in the list.
     */
    private static void printAllSchools(ArrayList<School> schoolList) {
        for (School school : schoolList) {
            System.err.println(school.getName() + ", " + school.getAddress());
        }
    }

    /**
     * A method that prints information about the Person objects added in the list.
     */
    private static void printAllPersons(School school) {
        for (Person person : school.getPersonList()) {
            System.err.println(person.getName() + ", " + person.getEmail() + ", " + person.getAge() + ", " + person.getGender());
        }
    }


    /**
     * A method that prints information about the Person objects using polymorphism.
     */
    private static void printAllStudentsAndAllTeachers(School school) {
        for (Person person : school.getPersonList()) {
            System.err.println(person.pretty());
        }
    }

}
